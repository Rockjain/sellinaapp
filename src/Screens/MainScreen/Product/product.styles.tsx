import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container:{
        flex:1,
        justifyContent:"center",
        alignItems:"center"
    },
    btnStyle:{
       marginTop:20,
       paddingHorizontal:20,
       paddingVertical:10,
       backgroundColor:"#0058FF",
       borderRadius:10 
    },
    btntxt:{
        color:"#FFF"
    }
})