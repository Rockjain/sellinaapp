import React from "react";
import { View, Text, Image } from "react-native";
import Basescreen from "../../../Component/General/Basescreen";
import styles from "./splash.style";
import TextInput from "../../../Component/General/TextInput";
import Button from "../../../Component/General/Button";
import Color from "../../../Common/Color";

const Logo = require("../../../Assets/Images/Logo.png");

class Splash extends React.Component {
  componentDidMount() {
    const { navigation } = this.props;
    setTimeout(() => {
      navigation.navigate("Login");
    }, 4000);
  }
  mainView() {
    return (
      <View>
        <Image
          source={Logo}
          style={{ height: 150, width: 115, alignSelf: "center" }}
        />
      </View>
    );
  }
  render() {
    return (
      <Basescreen style={styles.container}>
        <View>{this.mainView()}</View>
      </Basescreen>
    );
  }
}

export default Splash;
