import {StyleSheet,Dimensions} from 'react-native';
import React from 'react';
import Color from "../../../Common/Color";

const {width,height} = Dimensions.get("window")

export default StyleSheet.create({
    container:{
        justifyContent:"center",
        alignItems:"center"
    },
    textInputStyle:{
        borderRadius:6, borderColor:Color.primary,marginHorizontal:30, paddingVertical:10, borderWidth:1,
        width:width-25
    },
    inputMainStyle:{
        marginTop:15
    },
    btnMainView:{
        width:width-25,
        marginTop:20,
        justifyContent:"center",
        alignItems:"center",
        backgroundColor:Color.primary,
        borderRadius:4,
        paddingVertical:12,
        alignSelf:"center"
    },
    btnTextStyle:{
        color:Color.white
    }
})