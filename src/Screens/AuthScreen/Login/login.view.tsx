import React from "react";
import { View, Text, Image } from "react-native";
import Basescreen from "../../../Component/General/Basescreen";
import styles from "./login.style";
import TextInput from "../../../Component/General/TextInput";
import Button from "../../../Component/General/Button";
import Color from "../../../Common/Color";

const Logo = require("../../../Assets/Images/Logo.png")

interface Istate {
    username:string,
    password:string
}

class Login extends React.Component<{},Istate>{
    constructor(props){
        super(props)
        this.state={
            username:"",
            password:""
        }
    }

    renderBottomView(){
        return(
            <View>
                <Button style={styles.btnMainView} btnTitle={`Login`} textStyle={styles.btnTextStyle} onPress={()=>console.log("onClick")} />
        <Text style={{textAlign:'center',marginTop:12,fontSize:14,color:Color.primary}}>{`Forgot Password?`}</Text>
            </View>
        )
    }

    mainView(){
        const {username,password} = this.state;
        return(
            <View>
           <Image
           source={Logo}
           style={{height:150,width:115,alignSelf:"center"}}
           />
           <View style={styles.inputMainStyle}>
           <TextInput placeholderText={`User Name`} value={username} secureText={false} inputStyle={{marginLeft:10}} onChange={text=>this.setState({username:text})} 
           style={styles.textInputStyle}
           
           />
           <TextInput placeholderText={`Password`} value={username} secureText={true} inputStyle={{marginLeft:10}} onChange={text=>this.setState({password:text})} 
           style={[styles.textInputStyle,{marginTop:25}]}
           
           />
           {this.renderBottomView()}
           </View>
           
            </View>
        )
    }
    render(){
        return(
            <Basescreen style={styles.container}>
                <View>
                 {this.mainView()}
                 </View>
            </Basescreen>
        )
    }
}

export default Login;