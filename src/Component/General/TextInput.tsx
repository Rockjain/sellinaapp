import React from "react";
import { TextInput, View } from "react-native";

interface Iprops{
    style:object,
    inputStyle:object,
    value:string,
    onChange:Function,
    placeholderText:string,
    secureText:boolean
}

class Input extends React.Component<Iprops>{
    render(){
        const {style,inputStyle,value,onChange,placeholderText,secureText} = this.props;
        return(
           <View style={style}>
               <TextInput value={value} style={inputStyle} secureTextEntry={secureText} placeholder={placeholderText} onChangeText={text=>onChange(text)}  />
           </View>
        )
    }
}

export default Input;