import React from "react";
import { View, Text, SafeAreaView, StyleSheet } from "react-native";
import Color from "../../Common/Color";

interface Iprops{
    style:object
}

class BaseScreen extends React.Component<Iprops>{
    render(){
        const {style} = this.props
        return(
            <SafeAreaView style={[styles.container, style]}>
                {this.props.children}
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:Color.white
    }
})

export default BaseScreen;