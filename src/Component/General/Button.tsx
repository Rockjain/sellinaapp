import React from "react";
import { View, Text, TouchableOpacity } from "react-native";

interface Iprops {
    style:object,
    textStyle:object,
    onPress:Function,
    btnTitle:string
}

class Button extends React.Component<Iprops>{
     render(){
         const {style,textStyle,onPress,btnTitle} = this.props
         return(
             <TouchableOpacity style={style} onPress={onPress}>
                 <Text style={textStyle}>{btnTitle}</Text>
             </TouchableOpacity>
         )
     }
}

export default Button;