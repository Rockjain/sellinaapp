import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import ProductScreen from "../Screens/MainScreen/Product/product.view";
import LoginScreen from "../Screens/AuthScreen/Login/login.view";
import SplashScreen from "../Screens/AuthScreen/Splash/splash.view";

const AuthStack = createStackNavigator({
      Login:LoginScreen,
      Splash:SplashScreen
},
{
    headerMode:'none',
    initialRouteName:"Splash"
}
)

const Root = createSwitchNavigator({
    Product:ProductScreen,
    Auth:AuthStack,
},
{
    initialRouteName:"Auth"
})

const RootApp = createAppContainer(Root);
export default RootApp;